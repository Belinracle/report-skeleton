#ifndef __CONFIG_H__
#define __CONFIG_H__

#define BUFSIZE  1024
#define HISTORY_MAX_SIZE 100

#define VARIANT_NAME "var1"

#define DEV_FIRST_MAJOR 0
#define DEV_COUNT 1
#define ENTITIES 4

#endif // __CONFIG_H__